package com.example.kotlindistribution.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.kotlindistribution.Model.ListDriver
import com.example.kotlindistribution.R
import kotlinx.android.synthetic.main.item_list.view.*

class ListDriverAdapter(val driverList: ArrayList<ListDriver>, private val clickListener: (ListDriver) -> Unit, private val clickDelete: (ListDriver) -> Unit) :
    RecyclerView.Adapter<ListDriverAdapter.MyViewHolder>() {

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(listDriver: ListDriver, clickListener: (ListDriver) -> Unit, clickDelete:(ListDriver) -> Unit) {
            itemView.txt_name.text = listDriver.nama
            itemView.txt_jumlah.text = listDriver.jumlah
            itemView.txt_harga.text = listDriver.harga
            itemView.txt_alamat.text = listDriver.alamat
            itemView.btn_take.setOnClickListener{clickDelete(listDriver)}

            itemView.setOnClickListener{ clickListener (listDriver)}
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_list, parent, false)
        return MyViewHolder(v)
    }

    override fun getItemCount(): Int {
        return driverList.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(driverList[position], clickListener, clickDelete)
    }
}



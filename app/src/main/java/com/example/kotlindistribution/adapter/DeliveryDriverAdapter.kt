package com.example.kotlindistribution.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.kotlindistribution.Fragment.CompletedDeliveryFragment
import com.example.kotlindistribution.Fragment.DeliveryFragment
import com.example.kotlindistribution.Model.DeliveryDriver
import com.example.kotlindistribution.R
import kotlinx.android.synthetic.main.item_delivery.view.*

class DeliveryDriverAdapter(val driverDelivery: ArrayList<DeliveryDriver>, private val clickMapToko: (DeliveryDriver) -> Unit, private val clickMap: (DeliveryDriver) -> Unit, private val clickSelesai: (DeliveryDriver) -> Unit) :
    RecyclerView.Adapter<DeliveryDriverAdapter.ViewHolder>(){
    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        fun bind(deliveryDriver: DeliveryDriver,clickMapToko: (DeliveryDriver) -> Unit, clickMap: (DeliveryDriver) -> Unit, clickSelesai: (DeliveryDriver) -> Unit){
            itemView.txt_tokoPengambilan.text = deliveryDriver.tokoAmbil
            itemView.txt_alamatPengiriman.text = deliveryDriver.alamatKirim
            itemView.txt_yangdiambil.text = deliveryDriver.jumlahKirim
            itemView.txt_harga.text = deliveryDriver.biayaKirim
            itemView.btn_viewMapToko.setOnClickListener {clickMapToko(deliveryDriver)}
            itemView.btn_viewMap.setOnClickListener { clickMap(deliveryDriver) }
            itemView.btn_selesai.setOnClickListener { clickSelesai(deliveryDriver) }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_delivery, parent, false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return driverDelivery.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(driverDelivery[position],clickMapToko, clickMap, clickSelesai)
    }

}
package com.example.kotlindistribution

import android.app.AlertDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_login_driver.*
import kotlinx.android.synthetic.main.dialog_aktivasi.view.*

class LoginDriverActivity : AppCompatActivity() {

    private var userID: String = ""

    val mAuth = FirebaseAuth.getInstance()
    val db = FirebaseFirestore.getInstance()
    val mFirestore = db.collection("Pengguna")

    val verifikasi = "ExaDrQ123"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login_driver)

        val progressBar = findViewById<ProgressBar>(R.id.progressBar)

        //setting toolbar
        setSupportActionBar(findViewById(R.id.toolbar))
        //home navigation
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setTitle("")

        val buttonLogin = findViewById<View>(R.id.buttonLogin) as Button

        buttonLogin.setOnClickListener { view: View? ->
            progressBar.visibility = View.VISIBLE
            login()
        }

//        buttonLogin.setOnClickListener(View.OnClickListener {
//            val mDialogView = LayoutInflater.from(this).inflate(R.layout.dialog_aktivasi, null)
//            val mBuilder = AlertDialog.Builder(this)
//                .setView(mDialogView)
//                .setTitle("Masukan kode aktivasi anda")
//            val mAlertDialog = mBuilder.show()
//
//            mDialogView.btn_yes_aktivasi.setOnClickListener { view: View? ->
//                mAlertDialog.dismiss()
//
//                val aktivasi = mDialogView.et_aktivasi.text.toString().trim()
//                if( aktivasi == verifikasi)
//                    login()
//                else
//                    Toast.makeText(this, "Kode anda salah", Toast.LENGTH_SHORT).show()
//            }
//            mDialogView.btn_no_aktivasi.setOnClickListener { view: View? ->
//                mAlertDialog.dismiss()
//                Toast.makeText(this, "Akun anda belum aktif", Toast.LENGTH_SHORT).show()
//            }
//        })
    }

    private fun login() {
        val editLoginEmail = findViewById<View>(R.id.editLoginEmail) as EditText
        val editLoginPassword = findViewById<View>(R.id.editLoginPassword) as EditText

        val email = editLoginEmail.text.toString().trim()
        val password = editLoginPassword.text.toString().trim()

        if (email.isEmpty()) {
            Toast.makeText(this, "Email anda belum diisi", Toast.LENGTH_SHORT).show()
            progressBar.visibility = View.INVISIBLE
        } else if (password.isEmpty()) {
            Toast.makeText(this, "Password anda belum diisi", Toast.LENGTH_SHORT).show()
            progressBar.visibility = View.INVISIBLE
        } else {
            mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, OnCompleteListener { task ->
                    if (task.isSuccessful) {
                        val user = FirebaseAuth.getInstance().currentUser
                        userID = user!!.uid

                        val docRef = db.collection("Pengguna").document(userID)
                        docRef.get()
                            .addOnSuccessListener { document ->
                                finish()
                                if (document != null) {
                                    Log.d("Exist", "DocumentSnapshot data: ${document.data}")
                                    if (document.getString("roles") == "Driver") {
//                                        startActivity(Intent(this, HomePageDriverActivity::class.java))
                                        HomePageDriverActivity.launchIntentClearTask(this)
                                        Toast.makeText(
                                            this,
                                            "Berhasil Login :)",
                                            Toast.LENGTH_SHORT
                                        )
                                            .show()
                                    } else {
                                        Toast.makeText(
                                            this,
                                            "Roles anda tidak sesuai",
                                            Toast.LENGTH_LONG
                                        ).show()
                                    }
                                } else {
                                    Toast.makeText(this, "ID tidak ditemukan", Toast.LENGTH_LONG)
                                        .show()
                                    Log.d("No Exist", "No Document")
                                }
                            }
                    } else {
                        Toast.makeText(
                            this,
                            "Email dan Password Belum Terdaftar",
                            Toast.LENGTH_SHORT
                        ).show()
                        progressBar.visibility = View.INVISIBLE
                    }
                })
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    companion object {
        fun launchIntent(context: android.content.Context) {
            val intent = Intent(context, LoginDriverActivity::class.java)
            context.startActivity(intent)
        }

        fun launchIntentClearTask(context: android.content.Context) {
            val intent = Intent(context, LoginDriverActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK.or(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            context.startActivity(intent)
        }
    }
}

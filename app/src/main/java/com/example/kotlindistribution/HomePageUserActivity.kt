package com.example.kotlindistribution

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.example.kotlindistribution.Fragment.BalanceFragment
import com.example.kotlindistribution.Fragment.HomeFragment
import com.example.kotlindistribution.Fragment.ProfileFragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_home_page_user.*

class HomePageUserActivity : AppCompatActivity() {

    private var userID: String = ""

    val mAuth = FirebaseAuth.getInstance()
    val db = FirebaseFirestore.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home_page_user)

        setSupportActionBar(findViewById(R.id.toolbarUser))
        supportActionBar?.setTitle("")

        replaceFragment(HomeFragment())
        bottom_nav.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
    }

    private fun replaceFragment(fragment: Fragment?){
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        if (fragment != null) {
            fragmentTransaction.replace(R.id.fragmentContainer, fragment)
        }
        fragmentTransaction.commit()
    }

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        val actionBar =supportActionBar
        when(item.itemId) {
            R.id.home_menu -> {
                actionBar?.title = "Home"
                frame_layout.visibility = View.VISIBLE
                replaceFragment(HomeFragment())
                return@OnNavigationItemSelectedListener true
            }
            R.id.balance_menu -> {
                frame_layout.visibility = View.VISIBLE
                replaceFragment(BalanceFragment())
                actionBar?.title = "Balance"
                return@OnNavigationItemSelectedListener true
            }
            R.id.profile_menu ->{
                frame_layout.visibility = View.VISIBLE
                replaceFragment(ProfileFragment())
                actionBar?.title = "Profile"
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.nav_menu_home, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            R.id.nav_sign_out -> {
                logout()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun logout() {
        mAuth.signOut()
        finish()
        MainActivity.launchIntent(this)
    }

    companion object {
        fun launchIntent(context: android.content.Context) {
            val intent = Intent(context, HomePageUserActivity::class.java)
            context.startActivity(intent)
        }

        fun launchIntentClearTask(context: android.content.Context) {
            val intent = Intent(context, HomePageUserActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK.or(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            context.startActivity(intent)
        }
    }
}

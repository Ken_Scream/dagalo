package com.example.kotlindistribution

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView
import android.widget.Toast
import com.example.kotlindistribution.Common.Common
import com.example.kotlindistribution.Model.PlaceDetail
import com.example.kotlindistribution.Remote.IGoogleAPIService
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_view_place.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.Exception
import java.lang.StringBuilder

class ViewPlaceActivity : AppCompatActivity() {

    internal lateinit var mService: IGoogleAPIService
    var mPlace: PlaceDetail? = null

    private var userID: String = ""

    val mAuth = FirebaseAuth.getInstance()
    val db = FirebaseFirestore.getInstance()
    val mFirestore = db.collection("Pengguna")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view_place)

        //setting toolbar
        setSupportActionBar(findViewById(R.id.toolbar))
        //home navigation
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        mService = Common.googleApiService

        tv_placeName.text = ""
        tv_placeAddress.text = ""

        btn_save_market.setOnClickListener { view ->
            updateMarket()
            val intent = Intent(this, HomePageUserActivity::class.java)
            startActivity(intent)
        }

        mService.getDetailPlace(getPlaceDetailUrl(Common.currentResult!!.place_id))
            .enqueue(object : Callback<PlaceDetail> {
                override fun onFailure(call: Call<PlaceDetail>, t: Throwable) {
                    Toast.makeText(baseContext, "" + t.message, Toast.LENGTH_SHORT).show()
                }

                override fun onResponse(call: Call<PlaceDetail>, response: Response<PlaceDetail>) {
                    mPlace = response.body()

                    tv_placeAddress.text = mPlace!!.result!!.formated_address
                    tv_placeName.text = mPlace!!.result!!.name
                }
            })
    }

    private fun getPlaceDetailUrl(placeId: String?): String {
        val url = StringBuilder ("https://maps.googleapis.com/maps/api/place/details/json")
        url.append("?place_id=$placeId")
        url.append("&key=AIzaSyCMEEBEcP5ufVzrRUZm_rwKTdn2Hh5BN4U")
        return url.toString()
    }

    fun updateMarket(){
        val namaMarket = findViewById<View>(R.id.tv_placeName) as TextView
        val alamatMarket = findViewById<View>(R.id.tv_placeAddress) as TextView

        val nama = namaMarket.text.toString().trim()
        val alamat = alamatMarket.text.toString().trim()
        val user = FirebaseAuth.getInstance().currentUser
        userID = user!!.uid
        try{
            Log.d("Maps", nama+alamat)
            val user = mutableMapOf<String, Any>()
            user["namaToko"] = nama
            user["alamatToko"] = alamat

            mFirestore.document(userID).update(user).addOnSuccessListener {
                Toast.makeText(this, "Sukses", Toast.LENGTH_SHORT).show()
            }
        } catch (e: Exception){
            Toast.makeText(this, "Gagal", Toast.LENGTH_SHORT).show()
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}

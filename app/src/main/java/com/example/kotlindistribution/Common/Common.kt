package com.example.kotlindistribution.Common

import com.example.kotlindistribution.Model.Results
import com.example.kotlindistribution.Remote.IGoogleAPIService
import com.example.kotlindistribution.Remote.RetrofitClient

object Common {
    private val GOOGLE_API_URL="https://maps.googleapis.com/"

    var currentResult: Results?=null

    val googleApiService:IGoogleAPIService
        get() = RetrofitClient.getClient(GOOGLE_API_URL).create(IGoogleAPIService::class.java)
}
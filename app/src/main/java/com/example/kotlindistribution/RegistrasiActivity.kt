package com.example.kotlindistribution

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import android.widget.Toast.LENGTH_LONG
import androidx.core.view.isEmpty
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FieldValue
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_registrasi.*
import kotlinx.android.synthetic.main.activity_splash_screen.*
import java.lang.Exception

class RegistrasiActivity : AppCompatActivity() {

    private var userID: String = ""
    private var stringSpinner: String = ""

    var mAuth = FirebaseAuth.getInstance()
    val mFirestore = FirebaseFirestore.getInstance().collection("Pengguna")

    lateinit var notificationManager: NotificationManager
    lateinit var notificationChannel: NotificationChannel
    lateinit var builder: Notification.Builder
    private val channelId = "com.example.kotlindistribution"
    private val description = "Dagalo"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registrasi)

        //setting toolbar
        setSupportActionBar(findViewById(R.id.toolbar))
        //home navigation
        supportActionBar?.setTitle("")
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val buttonUntukRegister = findViewById<View>(R.id.buttonUntukRegister) as Button
        val buttonKirimKode = findViewById<View>(R.id.btn_kirimNotif) as Button
        val editKode = findViewById<View>(R.id.textViewKode) as TextView
        val isiKode = findViewById<View>(R.id.editTextCode) as EditText

        val progressBar = findViewById<ProgressBar>(R.id.PB)

        //ambil data dari resources
        val options = resources.getStringArray(R.array.option)
        //akses spinner
        val mSpinner = findViewById(R.id.spinnerUser) as Spinner
        ArrayAdapter.createFromResource(this, R.array.option, android.R.layout.simple_spinner_item)
            .also { adapter ->
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                mSpinner.adapter = adapter
            }

        mSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, pos: Int, id: Long) {
                if (pos == 0) {
                    false
                    editKode.visibility = View.INVISIBLE
                    isiKode.visibility = View.INVISIBLE
                    buttonKirimKode.visibility = View.INVISIBLE
                } else if (pos == 1) {
                    editKode.visibility = View.INVISIBLE
                    isiKode.visibility = View.INVISIBLE
                    buttonKirimKode.visibility = View.INVISIBLE
                    stringSpinner = parent?.getItemAtPosition(pos).toString().trim()
                    Log.d("Yang keambil", stringSpinner)
                } else {
                    editKode.visibility = View.VISIBLE
                    isiKode.visibility = View.VISIBLE
                    buttonKirimKode.visibility = View.VISIBLE
                    stringSpinner = parent?.getItemAtPosition(pos).toString().trim()
                    Log.d("Yang keambil", stringSpinner)
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                Toast.makeText(this@RegistrasiActivity, "Silahkan Pilih User/Driver", LENGTH_LONG)
                    .show()
            }
        }
        buttonUntukRegister.setOnClickListener { view: View? ->
            progressBar.visibility = View.VISIBLE
            saveRegister()
        }

        notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        buttonKirimKode.setOnClickListener { view: View? ->
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                notificationChannel =
                    NotificationChannel(channelId, description, NotificationManager.IMPORTANCE_HIGH)
                notificationChannel.enableLights(true)
                notificationChannel.lightColor = Color.GREEN
                notificationChannel.enableVibration(true)
                notificationManager.createNotificationChannel(notificationChannel)

                builder = Notification.Builder(this, channelId)
                    .setContentText(getString(R.string.kodeAktif))
                    .setContentTitle("Kode Aktivasi")
                    .setSmallIcon(R.drawable.ic_dagalo_trans)
                    .setLargeIcon(
                        BitmapFactory.decodeResource(
                            this.resources,
                            R.drawable.ic_dagalo_trans
                        )
                    )
            } else {
                builder = Notification.Builder(this)
                    .setContentText(getString(R.string.kodeAktif))
                    .setContentTitle("Kode AktiVasi")
                    .setSmallIcon(R.drawable.ic_dagalo_trans)
                    .setLargeIcon(
                        BitmapFactory.decodeResource(
                            this.resources,
                            R.drawable.ic_dagalo_trans
                        )
                    )
            }
            notificationManager.notify(123, builder.build())
        }

    }

    //save ke firestore
    private fun saveRegister() {
        val progressBar = findViewById<ProgressBar>(R.id.PB)
        //register ke auth+firestore
        val editNama = findViewById<View>(R.id.editNama) as EditText
        val editTextEmail = findViewById<View>(R.id.editTextEmail) as EditText
        val editTextPassword = findViewById<View>(R.id.editTextPassword) as EditText
        val editKode = findViewById<View>(R.id.editTextCode) as EditText

        val nama = editNama.text.toString().trim()
        val email = editTextEmail.text.toString().trim()
        val password = editTextPassword.text.toString().trim()
        val kode = editKode.text.toString().trim()

        Log.d("Yang mucul", stringSpinner)
        Log.d("kode", kode)
        if (nama.isEmpty()) {
            Toast.makeText(this, "Silahkan Isi Nama Anda", Toast.LENGTH_SHORT).show()
        } else if (email.isEmpty()) {
            Toast.makeText(this, "Silahkan Isi Email Anda", Toast.LENGTH_SHORT).show()
        } else if (password.isEmpty()) {
            Toast.makeText(this, "Silahkan Isi Password Anda", Toast.LENGTH_SHORT).show()
        } else if (stringSpinner.isEmpty()) {
            Toast.makeText(this, "Silahkan Pilih Peran Anda", Toast.LENGTH_SHORT).show()
        } else if (stringSpinner == "User") {
            this.mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener { task: Task<AuthResult> ->
                    if (task.isSuccessful) {
                        val user = FirebaseAuth.getInstance().currentUser
                        userID = user!!.uid
                        val docRef = mFirestore.document(userID)
                        try {
                            val user = HashMap<String, String>()
                            user.put("nama", nama)
                            user.put("email", email)
                            user.put("password", password)
                            user.put("roles", stringSpinner)

                            mFirestore.document(userID!!).set(user).addOnSuccessListener {
                                Toast.makeText(this, "Success", Toast.LENGTH_LONG).show()
                            }
                            MainActivity.launchIntentClearTask(this)
                        } catch (e: Exception) {
                            Toast.makeText(
                                this,
                                "Ada kesalahan input",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    } else {
                        Toast.makeText(applicationContext, "Input anda salah", LENGTH_LONG)
                            .show()
                    }
                }
        } else if (stringSpinner == "Driver") {
            if (kode.isEmpty()) {
                Toast.makeText(this, "Silahkan Isi Kode Anda", Toast.LENGTH_SHORT).show()
                progressBar.visibility = View.INVISIBLE
            } else if (kode == getString(R.string.kodeAktif)) {
                this.mAuth.createUserWithEmailAndPassword(email, password)
                    .addOnCompleteListener { task: Task<AuthResult> ->
                        if (task.isSuccessful) {
                            val user = FirebaseAuth.getInstance().currentUser
                            userID = user!!.uid
                            val docRef = mFirestore.document(userID)
                            Log.d("kode1", kode)
                            try {
                                val user = HashMap<String, String>()
                                user.put("nama", nama)
                                user.put("email", email)
                                user.put("password", password)
                                user.put("roles", stringSpinner)
                                user.put("kode", kode)
                                Log.d("kode2", kode)

                                mFirestore.document(userID).set(user).addOnSuccessListener {
                                    Toast.makeText(this, "Success", Toast.LENGTH_LONG).show()
                                }
                                //ambil dari field kode firestore
                                //hapus field kode kalau kosong
                                docRef.get().addOnSuccessListener { document ->
                                    val isiKode = document.getString("kode")
                                    if (isiKode.isNullOrEmpty()) {
                                        val deleteKode = hashMapOf<String, Any>(
                                            "kode" to FieldValue.delete()
                                        )
                                        docRef.update(deleteKode)
                                    }
                                }
                                MainActivity.launchIntentClearTask(this)
                            } catch (e: Exception) {
                                Toast.makeText(
                                    this,
                                    "Ada kesalahan input",
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                        } else {
                            Toast.makeText(applicationContext, "Input anda salah", LENGTH_LONG)
                                .show()
                        }
                    }
            } else {
                Toast.makeText(this, "Kode Aktivasi Tidak Sesuai", Toast.LENGTH_SHORT).show()
                progressBar.visibility = View.INVISIBLE
            }
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    companion object {
        fun launchIntent(context: Context) {
            val intent = Intent(context, RegistrasiActivity::class.java)
            context.startActivity(intent)
        }

        fun launchIntentClearTask(context: android.content.Context) {
            val intent = Intent(context, RegistrasiActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK.or(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            context.startActivity(intent)
        }
    }
}

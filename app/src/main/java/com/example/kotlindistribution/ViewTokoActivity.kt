package com.example.kotlindistribution

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.example.kotlindistribution.Fragment.DeliveryFragment
import com.example.kotlindistribution.Fragment.ListDriverFragment
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import java.lang.Exception
import com.example.kotlindistribution.MapsTokoActivity
import kotlinx.android.synthetic.main.activity_home_page_driver.*
import kotlinx.android.synthetic.main.activity_view_place.*
import kotlinx.android.synthetic.main.activity_view_toko.*

class ViewTokoActivity : AppCompatActivity() {

    private var userID: String = ""

    val mAuth = FirebaseAuth.getInstance()
    val db = FirebaseFirestore.getInstance()
    val mFirestore = db.collection("Pengguna")
    val mTransaksi = db.collection("Transaksi")

    private lateinit var tv_placeName: TextView
    private lateinit var tv_placeAddress: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view_toko)

        //setting toolbar
        setSupportActionBar(findViewById(R.id.toolbar))
        //home navigation
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        tv_placeName = findViewById(R.id.tv_placeName)
        tv_placeAddress = findViewById(R.id.tv_placeAddress)

        if(intent.extras != null)
        {
            val bundle = intent.extras
            tv_placeName.setText(bundle?.getString("value"))
            tv_placeAddress.setText(bundle?.getString("value1"))
        }else{
            tv_placeName.setText(intent.getStringExtra("value"))
            tv_placeAddress.setText(intent.getStringExtra("value1"))
        }
        val btn_pilih = findViewById<View>(R.id.btn_pilih_toko) as Button
        btn_pilih.setOnClickListener { view: View? ->
            updateMarket()
            val intent = Intent(this, HomePageDriverActivity::class.java)
            intent.putExtra("fragment", R.id.delivery_menu)
            startActivity(intent)
        }
    }

    fun updateMarket(){
        val namaMarket = findViewById<View>(R.id.tv_placeName) as TextView
        val alamatMarket = findViewById<View>(R.id.tv_placeAddress) as TextView

        val nama = namaMarket.text.toString().trim()
        val alamat = alamatMarket.text.toString().trim()
        val user = FirebaseAuth.getInstance().currentUser
        userID = user!!.uid
        val docRef = mFirestore.document(userID)
        docRef.get().addOnSuccessListener { document ->
            val A = document.getString("idTransaksi")
            val idTransaksi: String = A.toString()
            try {
                Log.d("Pilih", nama + alamat)
                val count = 4
                val F: String = count.toString()
                val user = mutableMapOf<String, Any>()
                user["namaToko"] = nama
                user["alamatToko"] = alamat
                user["countTimeLine"] = F

                mTransaksi.document(idTransaksi).update(user).addOnSuccessListener {
                    Toast.makeText(this, "Sukses", Toast.LENGTH_SHORT).show()
                }
            } catch (e: Exception) {
                Toast.makeText(this, "Gagal", Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}

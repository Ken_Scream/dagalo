package com.example.kotlindistribution

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.fragment.app.Fragment
import com.example.kotlindistribution.Fragment.BalanceDriverFragment
import com.example.kotlindistribution.Fragment.DeliveryFragment
import com.example.kotlindistribution.Fragment.ListDriverFragment
import com.example.kotlindistribution.Fragment.ProfileDriverFragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_home_page_driver.*

class HomePageDriverActivity : AppCompatActivity() {
    val mAuth = FirebaseAuth.getInstance()
    val db = FirebaseFirestore.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home_page_driver)

        setSupportActionBar(findViewById(R.id.toolbarDriver))

        if(intent.extras != null){
            val pilihan = intent.getIntExtra("fragment", R.id.delivery_menu)
            bottom_nav_driver.menu.findItem(pilihan).setChecked(true)
            bottom_nav_driver.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
            actionBar?.title = "Delivery"
            frame_layout_driver.visibility = View.VISIBLE
            replaceFragment(DeliveryFragment())
        } else {
            replaceFragment(ListDriverFragment())
            bottom_nav_driver.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        }
        bottom_nav_driver.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
    }

    private fun replaceFragment(fragment: Fragment?) {
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        if (fragment != null) {
            fragmentTransaction.replace(R.id.fragmentContainerDriver, fragment)
        }
        fragmentTransaction.commit()
    }


    private val mOnNavigationItemSelectedListener =
        BottomNavigationView.OnNavigationItemSelectedListener { item ->
            val actionBar = supportActionBar
            when (item.itemId) {
                R.id.list_menu -> {
                    actionBar?.title = "List"
                    frame_layout_driver.visibility = View.VISIBLE
                    replaceFragment(ListDriverFragment())
                    return@OnNavigationItemSelectedListener true
                }
                R.id.delivery_menu -> {
                    actionBar?.title = "Delivery"
                    frame_layout_driver.visibility = View.VISIBLE
                    replaceFragment(DeliveryFragment())
                    return@OnNavigationItemSelectedListener true
                }
                R.id.balance_menu -> {
                    actionBar?.title = "Balance"
                    frame_layout_driver.visibility = View.VISIBLE
                    replaceFragment(BalanceDriverFragment())
                    return@OnNavigationItemSelectedListener true
                }
                R.id.profile_menu -> {
                    actionBar?.title = "Profile"
                    frame_layout_driver.visibility = View.VISIBLE
                    replaceFragment(ProfileDriverFragment())
                    return@OnNavigationItemSelectedListener true
                }
            }
            false
        }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.nav_menu_home, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            R.id.nav_sign_out -> {
                logout()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun logout() {
        mAuth.signOut()
        finish()
        MainActivity.launchIntent(this)
    }

    companion object {
        fun launchIntent(context: android.content.Context) {
            val intent = Intent(context, HomePageDriverActivity::class.java)
            context.startActivity(intent)
        }

        fun launchIntentClearTask(context: android.content.Context) {
            val intent = Intent(context, HomePageDriverActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK.or(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            context.startActivity(intent)
        }
    }
}

package com.example.kotlindistribution

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Looper
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.example.kotlindistribution.Model.ListToko
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import java.io.IOException
import kotlin.time.seconds

class MapsTokoActivity : AppCompatActivity(), OnMapReadyCallback {

    private var nMap: GoogleMap? = null

    private var latitude: Double = 0.toDouble()
    private var longitude: Double = 0.toDouble()

    private lateinit var mLastLocation: Location
    private var mMarker: Marker? = null
    private val tokoMap = hashMapOf<LatLng, String>()

    //location
    lateinit var mFusedLocationClient: FusedLocationProviderClient
    lateinit var locationRequest: LocationRequest
    lateinit var locationCallback: LocationCallback

    val mAuth = FirebaseAuth.getInstance()
    val db = FirebaseFirestore.getInstance()
    val mFirestore = db.collection("Pengguna")
    val mToko = db.collection("Toko")
    val mTransaksi = db.collection("Transaksi")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps_toko)

        //setting toolbar
        setSupportActionBar(findViewById(R.id.toolbar))
        //home navigation
        supportActionBar?.setDisplayHomeAsUpEnabled(true)


        val btn_address = findViewById<View>(R.id.btn_addressToko) as Button

        btn_address.setOnClickListener { view: View? ->
            searchLocation(1)
        }
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.mapsToko) as SupportMapFragment
        mapFragment.getMapAsync(OnMapReadyCallback { nMap = it })

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkLocationPermission()) {
                buildLocationRequest()
                buildLocationCallBack()

                mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
                mFusedLocationClient.requestLocationUpdates(
                    locationRequest,
                    locationCallback,
                    Looper.myLooper()
                )
            } else {
                buildLocationRequest()
                buildLocationCallBack()

                mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
                mFusedLocationClient.requestLocationUpdates(
                    locationRequest,
                    locationCallback,
                    Looper.myLooper()
                )
            }
        }
    }

    fun buildLocationCallBack() {
        locationCallback = object : LocationCallback() {
            override fun onLocationResult(p0: LocationResult?) {
                mLastLocation = p0!!.locations.get(p0!!.locations.size - 1)

                if (mMarker != null) {
                    mMarker!!.remove()
                }

                latitude = mLastLocation.latitude
                longitude = mLastLocation.longitude
                val latLng = LatLng(latitude, longitude)
                Log.d("location", latLng.toString())
                val markerOptions = MarkerOptions()
                    .position(latLng)
                    .title("Your Position")
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED))
                mMarker = nMap!!.addMarker(markerOptions)
                nMap!!.moveCamera(CameraUpdateFactory.newLatLng(latLng))
                nMap!!.animateCamera(CameraUpdateFactory.zoomTo(15f))
            }
        }
    }

    fun showAddress() {
        val alamat = findViewById<TextView>(R.id.tv_addressToko)
        val user = FirebaseAuth.getInstance().currentUser
        val userID = user!!.uid
        val docRef = db.collection("Pengguna").document(userID)
        docRef.get()
            .addOnSuccessListener { document ->
                val A = document.getString("idTransaksi")
                val idTransaksi: String = A.toString()
                val docRef1 = mTransaksi.document(idTransaksi)
                docRef1.get().addOnSuccessListener { document ->
                    alamat.text = document.getString("alamatPemesan")
                }
                lateinit var location: String
                location = alamat.text.toString()
                var addressList: List<Address>? = null

                if (location == null || location == "") {
//            Toast.makeText(applicationContext,"Show location",Toast.LENGTH_SHORT).show()
                } else {
                    val geoCoder = Geocoder(this)
                    try {
                        addressList = geoCoder.getFromLocationName(location, 1)
                    } catch (e: IOException) {
                        e.printStackTrace()
                    }
                    val address = addressList!![0]
                    val latLng = LatLng(address.latitude, address.longitude)
                    nMap!!.addMarker(
                        MarkerOptions().position(latLng).title(location).icon(
                            BitmapDescriptorFactory.defaultMarker(
                                BitmapDescriptorFactory.HUE_GREEN
                            )
                        )
                    )
                    nMap!!.animateCamera(CameraUpdateFactory.newLatLng(latLng))
                    nMap!!.animateCamera(CameraUpdateFactory.zoomTo(15f))
                }
            }
    }

    fun searchLocation(size: Int) {
        showAddress()
        Toast.makeText(this, "Klik 1x lagi untuk alamat pemesan", Toast.LENGTH_SHORT).show()
        val listToko = ArrayList<ListToko>()
        mToko.get().addOnSuccessListener { result ->
            for (document in result) {
                val namaToko = document.getString("namaToko")
                val alamatToko = document.getString("alamatToko")
                val latitudeToko = document.getString("latitude")
                val longitudeToko = document.getString("longitude")
                val stock = document.getString("stock")
                Log.d("Order", "${document.id} => ${document.data}")
                for (i in 0 until size) {
                    val item = ListToko(
                        "$namaToko",
                        "$alamatToko",
                        "$latitudeToko",
                        "$longitudeToko",
                        "$stock"
                    )

                    val markerOptions = MarkerOptions()
                    val latToko = latitudeToko
                    val lat: Double = latToko!!.toDouble()
                    val lngToko = longitudeToko
                    val lng: Double = lngToko!!.toDouble()
                    val placeName = namaToko
                    val name: String = placeName.toString()
                    val addressPlace = alamatToko
                    val address: String? = addressPlace.toString()
//                    val stockPlace = stock
                    val latLng = LatLng(lat, lng)
                    listToko.add(item)
//                    Log.d("qwer", listToko.get(i).toString())

                    markerOptions.position(latLng).title(name).snippet(addressPlace)
                        .icon(
                            BitmapDescriptorFactory.defaultMarker(
                                BitmapDescriptorFactory.HUE_BLUE
                            )
                        )

                    nMap!!.setOnMarkerClickListener { marker ->
                        val namaT = marker.title
                        val addressT = marker.snippet
                        val bundle = Bundle()
                        bundle.putString("value", namaT)
                        bundle.putString("value1", addressT)
                        finish()
                        val intent = Intent(this, ViewTokoActivity::class.java)
                        intent.putExtras(bundle)
                        this.startActivity(intent)
                        true
                    }

                    nMap!!.addMarker(markerOptions)
                    nMap!!.animateCamera(CameraUpdateFactory.zoomTo(15f))
                }
            }
        }
    }

    private fun buildLocationRequest() {
        locationRequest = LocationRequest()
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        locationRequest.interval = 5000
        locationRequest.fastestInterval = 3000
        locationRequest.smallestDisplacement = 15f
    }

    private fun checkLocationPermission(): Boolean {
        if (ContextCompat.checkSelfPermission(
                this,
                android.Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    this,
                    Manifest.permission.ACCESS_FINE_LOCATION
                )
            )
                ActivityCompat.requestPermissions(
                    this, arrayOf(
                        android.Manifest.permission.ACCESS_FINE_LOCATION
                    ), MY_PERMISSION_CODE
                )
            else
                ActivityCompat.requestPermissions(
                    this, arrayOf(
                        android.Manifest.permission.ACCESS_FINE_LOCATION
                    ), MY_PERMISSION_CODE
                )
            return false
        } else
            return true
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            MY_PERMISSION_CODE -> {
                if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ContextCompat.checkSelfPermission(
                            this,
                            android.Manifest.permission.ACCESS_FINE_LOCATION
                        ) == PackageManager.PERMISSION_GRANTED
                    )
                        if (checkLocationPermission())
                            buildLocationRequest()
                    buildLocationCallBack()

                    mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
                    mFusedLocationClient.requestLocationUpdates(
                        locationRequest,
                        locationCallback,
                        Looper.myLooper()
                    )
                    nMap!!.isMyLocationEnabled = true
                }
            }
            else -> {
                Toast.makeText(this, "Permission Denied", Toast.LENGTH_LONG).show()
            }
        }
    }

    override fun onStop() {
        mFusedLocationClient.removeLocationUpdates(locationCallback)
        super.onStop()
    }

    override fun onMapReady(googleMap: GoogleMap) {
        nMap = googleMap

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(
                    this,
                    android.Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                nMap!!.isMyLocationEnabled = true
            }
        } else
            nMap!!.isMyLocationEnabled = true
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
    companion object {
        private const val MY_PERMISSION_CODE = 1000
    }
}

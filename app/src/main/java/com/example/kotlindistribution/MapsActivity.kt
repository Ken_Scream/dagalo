package com.example.kotlindistribution

import android.location.Location
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import android.location.LocationManager
import android.os.Build
import android.os.Looper
import android.provider.Settings
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.model.*
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_maps.*
import java.io.IOException

class MapsActivity : AppCompatActivity(), OnMapReadyCallback {

    private var nMap: GoogleMap? = null
    private lateinit var geocoder: Geocoder

    private var latitude: Double = 0.toDouble()
    private var longitude: Double = 0.toDouble()

    private lateinit var mLastLocation: Location
    private var mMarker: Marker? = null

    //location
    lateinit var mFusedLocationClient: FusedLocationProviderClient
    lateinit var locationRequest: LocationRequest
    lateinit var locationCallback: LocationCallback

    val mAuth = FirebaseAuth.getInstance()
    val db = FirebaseFirestore.getInstance()
    val mFirestore = db.collection("Pengguna")
    val mTransaksi = db.collection("Transaksi")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)

        //setting toolbar
        setSupportActionBar(findViewById(R.id.toolbar))
        //home navigation
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val btn_address = findViewById<View>(R.id.btn_address) as Button
        val btn_toko = findViewById<View>(R.id.btn_toko) as Button

        btn_address.setOnClickListener { view: View? ->
            searchLocation(view!!)
        }

        btn_toko.setOnClickListener { view: View? ->
            searchLocation1(view!!)
        }


        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.maps) as SupportMapFragment
        mapFragment.getMapAsync(OnMapReadyCallback { nMap = it })

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkLocationPermission()) {
                buildLocationRequest()
                buildLocationCallBack()

                mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
                mFusedLocationClient.requestLocationUpdates(
                    locationRequest,
                    locationCallback,
                    Looper.myLooper()
                )
            } else {
                buildLocationRequest()
                buildLocationCallBack()

                mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
                mFusedLocationClient.requestLocationUpdates(
                    locationRequest,
                    locationCallback,
                    Looper.myLooper()
                )
            }
        }
    }

    fun buildLocationCallBack() {
        locationCallback = object : LocationCallback() {
            override fun onLocationResult(p0: LocationResult?) {
                mLastLocation = p0!!.locations.get(p0!!.locations.size - 1)

                if (mMarker != null) {
                    mMarker!!.remove()
                }

                latitude = mLastLocation.latitude
                longitude = mLastLocation.longitude
                val latLng = LatLng(latitude, longitude)
                Log.d("location", latLng.toString())
                val markerOptions = MarkerOptions()
                    .position(latLng)
                    .title("Your Position")
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED))
                mMarker = nMap!!.addMarker(markerOptions)

                nMap!!.moveCamera(CameraUpdateFactory.newLatLng(latLng))
                nMap!!.animateCamera(CameraUpdateFactory.zoomTo(15f))

            }

        }
    }

    fun searchLocation(view: View) {
        val locationSearch: TextView = findViewById<EditText>(R.id.tv_address)
        val user = FirebaseAuth.getInstance().currentUser
        val userID = user!!.uid
        val docRef = db.collection("Pengguna").document(userID)
        docRef.get()
            .addOnSuccessListener { document ->
                val A = document.getString("idTransaksi")
                val idTransaksi: String = A.toString()
                val docRef1 = mTransaksi.document(idTransaksi)
                docRef1.get().addOnSuccessListener { document ->
                    if (document != null) {
                        locationSearch.text = document.getString("alamatPemesan")
                    } else {
                        Log.d("No Exist", "No Document")
                    }
                }
            }
        lateinit var location: String
        location = locationSearch.text.toString()

        var addressList: List<Address>? = null

        if (location == null || location == "") {
//            Toast.makeText(applicationContext,"Show location",Toast.LENGTH_SHORT).show()
        } else {
            val geoCoder = Geocoder(this)
            try {
                addressList = geoCoder.getFromLocationName(location, 1)
            } catch (e: IOException) {
                e.printStackTrace()
            }
            val address = addressList!![0]
            val latLng = LatLng(address.latitude, address.longitude)
            nMap!!.addMarker(
                MarkerOptions().position(latLng).title(location).icon(
                    BitmapDescriptorFactory.defaultMarker(
                        BitmapDescriptorFactory.HUE_GREEN
                    )
                )
            )
            nMap!!.animateCamera(CameraUpdateFactory.newLatLng(latLng))
            nMap!!.animateCamera(CameraUpdateFactory.zoomTo(15f))
            Toast.makeText(
                applicationContext,
                address.latitude.toString() + " " + address.longitude,
                Toast.LENGTH_LONG
            ).show()
        }
    }

    private fun searchLocation1(view: View) {
        val locationSearch1 = findViewById<EditText>(R.id.tv_address1) as TextView
        val locationSearch2 = findViewById<EditText>(R.id.tv_address2) as TextView
        val user = FirebaseAuth.getInstance().currentUser
        val userID = user!!.uid
        val docRef = db.collection("Pengguna").document(userID)
        docRef.get()
            .addOnSuccessListener { document ->
                val A = document.getString("idTransaksi")
                val idTransaksi: String = A.toString()
                val docRef1 = mTransaksi.document(idTransaksi)
                docRef1.get().addOnSuccessListener { document ->
                    if (document != null) {
                        locationSearch1.text = document.getString("namaToko")
                        locationSearch2.text = document.getString("alamatToko")
                    } else {
                        Log.d("No Exist", "No Document")
                    }
                }
            }
        lateinit var location1: String
        location1 = locationSearch1.text.toString()

        lateinit var address1: String
        address1 = locationSearch2.text.toString()

        var addressList1: List<Address>? = null

        if (address1 == null || address1 == "") {
//            Toast.makeText(applicationContext,"Show location",Toast.LENGTH_SHORT).show()
        } else {
            val geoCoder = Geocoder(this)
            try {
                addressList1 = geoCoder.getFromLocationName(address1, 1)
                Log.d("geocoder", addressList1.toString())
            } catch (e: IOException) {
                e.printStackTrace()
            }
            Log.d("pilih", location1)
            val address = addressList1!![0]
            Log.d("corn", address.toString())
            val latLng = LatLng(address.latitude, address.longitude)
            nMap!!.addMarker(
                MarkerOptions().position(latLng).title(location1).snippet(address1).icon(
                    BitmapDescriptorFactory.defaultMarker(
                        BitmapDescriptorFactory.HUE_BLUE
                    )
                )
            )
            nMap!!.animateCamera(CameraUpdateFactory.newLatLng(latLng))
            nMap!!.animateCamera(CameraUpdateFactory.zoomTo(15f))
            Toast.makeText(
                applicationContext,
                address.latitude.toString() + " " + address.longitude,
                Toast.LENGTH_LONG
            ).show()
        }
    }

    private fun buildLocationRequest() {
        locationRequest = LocationRequest()
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        locationRequest.interval = 5000
        locationRequest.fastestInterval = 3000
        locationRequest.smallestDisplacement = 15f
    }

    private fun checkLocationPermission(): Boolean {
        if (ContextCompat.checkSelfPermission(
                this,
                android.Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    this,
                    Manifest.permission.ACCESS_FINE_LOCATION
                )
            )
                ActivityCompat.requestPermissions(
                    this, arrayOf(
                        android.Manifest.permission.ACCESS_FINE_LOCATION
                    ), MY_PERMISSION_CODE
                )
            else
                ActivityCompat.requestPermissions(
                    this, arrayOf(
                        android.Manifest.permission.ACCESS_FINE_LOCATION
                    ), MY_PERMISSION_CODE
                )
            return false
        } else
            return true
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            MY_PERMISSION_CODE -> {
                if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ContextCompat.checkSelfPermission(
                            this,
                            android.Manifest.permission.ACCESS_FINE_LOCATION
                        ) == PackageManager.PERMISSION_GRANTED
                    )
                        if (checkLocationPermission())
                            buildLocationRequest()
                    buildLocationCallBack()

                    mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
                    mFusedLocationClient.requestLocationUpdates(
                        locationRequest,
                        locationCallback,
                        Looper.myLooper()
                    )
                    nMap!!.isMyLocationEnabled = true
                }
            }
            else -> {
                Toast.makeText(this, "Permission Denied", Toast.LENGTH_LONG).show()
            }
        }
    }

    override fun onStop() {
        mFusedLocationClient.removeLocationUpdates(locationCallback)
        super.onStop()
    }

    override fun onMapReady(googleMap: GoogleMap) {
        nMap = googleMap

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(
                    this,
                    android.Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                nMap!!.isMyLocationEnabled = true
            }
        } else
            nMap!!.isMyLocationEnabled = true
        nMap!!.uiSettings.isZoomControlsEnabled = true
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    companion object {
        private const val MY_PERMISSION_CODE = 1000
    }
}

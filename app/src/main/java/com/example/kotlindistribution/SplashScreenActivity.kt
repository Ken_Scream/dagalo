package com.example.kotlindistribution

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.widget.ProgressBar
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_splash_screen.*
import java.util.*
import kotlin.concurrent.timerTask

class SplashScreenActivity : AppCompatActivity() {

    private val SPLASH_TIME_OUT = 3000L

    private var userID: String = ""

    val mAuth = FirebaseAuth.getInstance()
    val db = FirebaseFirestore.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)

        val progressBar = findViewById<View>(R.id.progressBar1) as ProgressBar

        Handler().postDelayed({
            //do something after 3000ms
            finish()
            checkUserAccountSignIn()
            progressBar.visibility = View.VISIBLE
        }, SPLASH_TIME_OUT)
    }

    private fun checkUserAccountSignIn() {
        if(mAuth.currentUser != null) {
            finish()
            Log.d("User", mAuth.currentUser.toString())
            val user = FirebaseAuth.getInstance().currentUser
            userID = user!!.uid
            val docRef = db.collection("Pengguna").document(userID)
            docRef.get()
                .addOnSuccessListener { document ->
                    if(document != null){
                        Log.d("Ini isinya", document.getString("roles"))
                        if(document.getString("roles") == "User")
                            HomePageUserActivity.launchIntentClearTask(this)
                        if(document.getString("roles") == "Driver")
                            HomePageDriverActivity.launchIntentClearTask(this)
                    }
                }
        }else{
            MainActivity.launchIntentClearTask(this)
        }
    }
}

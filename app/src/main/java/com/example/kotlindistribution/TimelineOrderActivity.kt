package com.example.kotlindistribution

import android.content.Context
import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_timeline_order.*
import params.com.stepview.StatusViewScroller
import pl.hypeapp.materialtimelineview.MaterialTimelineView

class TimelineOrderActivity : AppCompatActivity() {
    private var userID: String = ""
    var count: String = ""

    val mAuth = FirebaseAuth.getInstance()
    val db = FirebaseFirestore.getInstance()
    val mFirestore = db.collection("Pengguna")
    val mTransaksi = db.collection("Transaksi")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_timeline_order)

        //setting toolbar
        setSupportActionBar(findViewById(R.id.toolbar))
        //home navigation
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setTitle("Detail Pemesanan")

        showTransaksi()
        namaToko()
    }

    fun transaksi(){
        val StatusViewScroller = findViewById<StatusViewScroller>(R.id.statusViewScroller)
        StatusViewScroller.statusView.run {
            val user = FirebaseAuth.getInstance().currentUser
            userID = user!!.uid
            val docRef = mFirestore.document(userID)
            docRef.get().addOnSuccessListener { document ->
                val id = document.getString("idTransaksi")
                val idTransaksi: String = id.toString()
                Log.d("count", idTransaksi)

                val docRef1 = mTransaksi.document(idTransaksi)
                docRef1.get().addOnSuccessListener { document ->
                    val countTime = document.getString("countTimeLine")
                    Log.d("count", count)
                    count = countTime.toString()
                    val count1: Int = count.toInt()
                    currentCount = count1
                    circleFillColorCurrent = Color.rgb(0,75,141)
                }
            }
        }
    }

    fun showTransaksi(){
        val namaText = findViewById<TextView>(R.id.txt_nama_penerima)
        val alamatText = findViewById<TextView>(R.id.txt_alamat_penerima)
        val user = FirebaseAuth.getInstance().currentUser
        userID = user!!.uid
        val docRef = mFirestore.document(userID)
        docRef.get()
            .addOnSuccessListener { document ->
                if (document != null) {
                    val A = document.getString("idTransaksi")
                    val idTransaksi: String = A.toString()

                    if (A != null){
                        txt_tidak_ada.visibility = View.INVISIBLE
                        val docRef1 = mTransaksi.document(idTransaksi)
                        docRef1.get().addOnSuccessListener { document ->
                            namaText.text = document.getString("namaPemesan")
                            alamatText.text = document.getString("alamatPemesan")
                            transaksi()
                        }
                    } else {
                        isi_transaksi.visibility = View.INVISIBLE
                    }
                } else {
                    Log.d("No Exist", "No Document")
                }
            }
    }

    fun namaToko(){
        transaksi()
        val namaText = findViewById<TextView>(R.id.txt_nama_toko)
        val user = FirebaseAuth.getInstance().currentUser
        userID = user!!.uid
        val docRef = mFirestore.document(userID)
        docRef.get()
            .addOnSuccessListener { document ->
                if (document != null) {
                    val A = document.getString("idTransaksi")
                    val idTransaksi: String = A.toString()

                    if (A != null){
                        txt_tidak_ada.visibility = View.INVISIBLE
                        val docRef1 = mTransaksi.document(idTransaksi)
                        docRef1.get().addOnSuccessListener { document ->
                            namaText.text = document.getString("namaToko")
                        }
                    } else {
                        isi_transaksi.visibility = View.INVISIBLE
                    }
                } else {
                    Log.d("No Exist", "No Document")
                }
            }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}

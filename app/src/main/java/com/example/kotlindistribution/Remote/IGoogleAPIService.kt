package com.example.kotlindistribution.Remote

import com.example.kotlindistribution.Model.MyPlaces
import com.example.kotlindistribution.Model.PlaceDetail
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Url

interface IGoogleAPIService {
    @GET
    fun getNearbyPlace(@Url url: String): Call<MyPlaces>

    @GET
    fun getDetailPlace(@Url url:String): Call<PlaceDetail>
}
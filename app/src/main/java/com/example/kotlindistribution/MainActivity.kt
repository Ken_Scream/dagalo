package com.example.kotlindistribution

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.*
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var buttonUser: Button
    private lateinit var buttonDriver: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        buttonUser = findViewById(R.id.button_login_user)
        buttonUser.setOnClickListener{
//            LoginActivity.launchIntent(this)
            startActivity(Intent(this, LoginActivity::class.java))
        }

        buttonDriver = findViewById(R.id.button_login_driver)
        buttonDriver.setOnClickListener{
//            LoginDriverActivity.launchIntent(this)
            startActivity(Intent(this, LoginDriverActivity::class.java))
        }

        textNotHave.setOnClickListener {
            RegistrasiActivity.launchIntent(this)
        }
    }



    companion object{
        fun launchIntent(context: Context){
            val intent = Intent(context, MainActivity::class.java)
            context.startActivity(intent)
        }
        fun launchIntentClearTask(context: Context) {
            val intent = Intent(context, MainActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK.or(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            context.startActivity(intent)
        }
    }
}

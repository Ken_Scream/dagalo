package com.example.kotlindistribution

import android.content.Intent
import android.os.Bundle
import android.provider.ContactsContract
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_login.progressBar
import kotlinx.android.synthetic.main.activity_login_driver.*
import kotlinx.android.synthetic.main.activity_main.*

class LoginActivity: AppCompatActivity(){

    private var userID: String = ""

    val mAuth = FirebaseAuth.getInstance()
    val db = FirebaseFirestore.getInstance()
    val mFirestore = db.collection("Pengguna")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        val progressBar = findViewById<ProgressBar>(R.id.progressBar)

        //setting toolbar
        setSupportActionBar(findViewById(R.id.toolbar))
        //home navigation
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setTitle("")

        val buttonLogin = findViewById<View>(R.id.buttonLogin) as Button

        buttonLogin.setOnClickListener(View.OnClickListener {
            progressBar.visibility = View.VISIBLE
            login()
        })
    }

    private fun login() {
        val editLoginEmail = findViewById<View>(R.id.editLoginEmail) as EditText
        val editLoginPassword = findViewById<View>(R.id.editLoginPassword) as EditText

        val email = editLoginEmail.text.toString()
        val password = editLoginPassword.text.toString()

        if (email.isEmpty()) {
            Toast.makeText(this, "Email anda belum diisi", Toast.LENGTH_SHORT).show()
            progressBar.visibility = View.INVISIBLE
        } else if (password.isEmpty()) {
            Toast.makeText(this, "Password anda belum diisi", Toast.LENGTH_SHORT).show()
            progressBar.visibility = View.INVISIBLE
        } else{
            mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(this, OnCompleteListener { task ->
                if(task.isSuccessful){
                    val user = FirebaseAuth.getInstance().currentUser
                    userID = user!!.uid

                    val docRef = mFirestore.document(userID)
                    docRef.get()
                        .addOnSuccessListener { document ->
                            finish()
                            if (document != null) {
                                Log.d("Exist", "DocumentSnapshot data: ${document.data}")
                                if (document.getString("roles") == "User"){
//                                    startActivity(Intent(this, HomePageUser::class.java))
                                    check()
                                    HomePageUserActivity.launchIntentClearTask(this)
                                    Toast.makeText(this, "Berhasil Login :)", Toast.LENGTH_SHORT)
                                        .show()
                                } else {
                                    Toast.makeText(this, "Roles anda tidak sesuai", Toast.LENGTH_LONG).show()
                                }
                            } else {
                                Log.d("No Exist", "No Document")
                            }
                        }
                } else{
                    Toast.makeText(this, "Email atau Password salah :(", Toast.LENGTH_SHORT).show()
                    progressBar.visibility = View.INVISIBLE
                }
            })
        }
    }

    fun check() {
        val user = FirebaseAuth.getInstance().currentUser
        userID = user!!.uid
        val docRef = mFirestore.document(userID).collection("History")
        docRef.get().addOnSuccessListener { result ->
            for (document in result) {
                val A = document.getString("hargaPesanan")
                val B: String = A.toString()
//            Log.d("History", "Masuk")
                Log.d("History", B)
                Log.d("History", "${document.id} => ${document.data}")
            }
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    companion object {
        fun launchIntent(context: android.content.Context) {
            val intent = Intent(context, LoginActivity::class.java)
            context.startActivity(intent)
        }
        fun launchIntentClearTask(context: android.content.Context) {
            val intent = Intent(context, LoginActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK.or(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            context.startActivity(intent)
        }
    }
}
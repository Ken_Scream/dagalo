package com.example.kotlindistribution.Model

import com.example.kotlindistribution.adapter.ViewType

data class GalonStatus(
    val namaDriver : String) : ViewType{
    override fun getViewType(): Int = ViewType.HEADER
}
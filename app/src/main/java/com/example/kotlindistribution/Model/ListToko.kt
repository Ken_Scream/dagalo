package com.example.kotlindistribution.Model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ListToko (
    val namaToko: String,
    val alamatToko: String,
    val latitudeToko: String,
    val longitudeToko: String,
    val stock: String): Parcelable
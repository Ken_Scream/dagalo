package com.example.kotlindistribution.Model

class Results {

    var name:String?=null
    var icon:String?=null
    var geometry:Geometry?=null
    val id:String?=null
    val place_id:String?=null
    val price_level:Int?=0
    var reference:String?=null
    var scope:String?=null
    var types:Array<String>?=null
    var vicinity:String?=null

    var address_components:Array<AddressComponent>?=null
    var adr_address:String?=null
    var formated_address:String?=null
    var formated_phone_number:String?=null
    var international_phone_number:String?=null
    var url:String?=null
}
package com.example.kotlindistribution.Model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ListDriver(
    val id: String,
    val nama: String,
    val jumlah: String,
    val harga: String,
    val alamat: String,
    val namaToko: String,
    val countTimeLine: String) : Parcelable

package com.example.kotlindistribution.Model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class DeliveryDriver(
    val tokoAmbil: String,
    val alamatKirim: String,
    val jumlahKirim: String,
    val biayaKirim: String) : Parcelable

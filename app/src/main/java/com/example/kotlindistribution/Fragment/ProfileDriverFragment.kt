package com.example.kotlindistribution.Fragment


import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast

import com.example.kotlindistribution.R
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.fragment_profile_driver.*
import java.lang.Exception

/**
 * A simple [Fragment] subclass.
 */
class ProfileDriverFragment : Fragment() {
    private var userID: String = ""

    val mAuth = FirebaseAuth.getInstance()
    val db = FirebaseFirestore.getInstance()
    val mFirestore = db.collection("Pengguna")


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_profile_driver, container, false)
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        showProfile()

        btn_save_profile_driver.setOnClickListener { view: View? ->
            updateProfile()
        }
    }

    fun showProfile() {
        val namaText = view?.findViewById<View>(R.id.et_nama_profile_driver) as TextView
        val emailText = view?.findViewById<View>(R.id.et_email_profile_driver) as TextView
        val passwordText = view?.findViewById<View>(R.id.et_password_profile_driver) as TextView
        val kapasitasText = view?.findViewById<View>(R.id.et_minimal_profile_driver) as TextView

        val user = FirebaseAuth.getInstance().currentUser
        userID = user!!.uid
        val docRef = db.collection("Pengguna").document(userID)
        docRef.get()
            .addOnSuccessListener { document ->
                if (document != null) {
                    Log.d("Exist", "DocumentSnapshot data: ${document.data}")
                    namaText.text = document.getString("nama")
                    emailText.text = document.getString("email")
                    passwordText.text = document.getString("password")
                    kapasitasText.text = document.getString("kapasitas")
                } else {
                    Log.d("No Exist", "No Document")
                }
            }
    }

    fun updateProfile(){
        val namaText = view?.findViewById<View>(R.id.et_nama_profile_driver) as EditText
        val emailText = view?.findViewById<View>(R.id.et_email_profile_driver) as EditText
        val passwordText = view?.findViewById<View>(R.id.et_password_profile_driver) as EditText
        val jumlahKapastitas = view?.findViewById<View>(R.id.et_minimal_profile_driver) as EditText

        val nama = namaText.text.toString().trim()
        val email = emailText.text.toString().trim()
        val password = passwordText.text.toString().trim()
        val kapasitas = jumlahKapastitas.text.toString().trim()
        val user = FirebaseAuth.getInstance().currentUser

        if(nama.isEmpty() && email.isEmpty() && password.isEmpty() && kapasitas.isEmpty()){
            Toast.makeText(context,"Tolong isi Nama/email/password/Jumlah yang diinginkan", Toast.LENGTH_SHORT).show()
        }
        else{
            user?.updateEmail(email)
                ?.addOnCompleteListener{ task ->
                    if (task.isSuccessful) {
                        Log.d("Profile", "User email address updated.")
                    }
                }
            user?.updatePassword(password)
                ?.addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        Log.d("Profile", "User password updated.")
                    }
                }

            userID = user!!.uid
            try{
                Log.d("jumlah yang diinginkan", kapasitas)
                val user = mutableMapOf<String, Any>()
                user["nama"] = nama
                user["email"] = email
                user["password"] = password
                user["kapasitas"] = kapasitas
                user["kapasitasBerubah"] = kapasitas

                mFirestore.document(userID).update(user).addOnSuccessListener{
                    Toast.makeText(context, "Sukses", Toast.LENGTH_SHORT).show()
                }

            } catch (e: Exception){
                Toast.makeText(context, "Gagal", Toast.LENGTH_SHORT).show()
            }
        }
    }

    companion object {
        fun getInstance(): ProfileDriverFragment = ProfileDriverFragment()
    }
}

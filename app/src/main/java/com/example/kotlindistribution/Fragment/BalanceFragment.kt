package com.example.kotlindistribution.Fragment


import android.app.AlertDialog
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast

import com.example.kotlindistribution.R
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.dialog_topup.view.*
import kotlinx.android.synthetic.main.fragment_balance.*
import java.lang.Exception

/**
 * A simple [Fragment] subclass.
 */
class BalanceFragment : Fragment() {
    private var userID: String = ""

    val mAuth = FirebaseAuth.getInstance()
    val db = FirebaseFirestore.getInstance()
    val mFirestore = db.collection("Pengguna")
    val mOrder = db.collection("Orderan")

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_balance, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getProfile()
        hitung()

        btn_topUp.setOnClickListener { view: View? ->
            val mDialogView = LayoutInflater.from(context).inflate(R.layout.dialog_topup, null)
            val mBuilder = AlertDialog.Builder(context)
                .setView(mDialogView)
                .setTitle("Top Up Saldo")
            val mAlertDialog = mBuilder.show()

            mDialogView.btn_yes_topup.setOnClickListener { view: View? ->
                mAlertDialog.dismiss()
                val saldoTopUp = mDialogView.et_topup.text.toString().trim()
                val user = FirebaseAuth.getInstance().currentUser
                userID = user!!.uid
                if (saldoTopUp.isNullOrEmpty()) {
                    Toast.makeText(context, "Silahkan isi saldo", Toast.LENGTH_SHORT).show()
                } else {
                    val docRef = db.collection("Pengguna").document(userID)
                    docRef.get()
                        .addOnSuccessListener { document ->
                            val saldoFirestore = document.getString("saldo")
                            var saldosementara: Int? = saldoFirestore?.toIntOrNull()
                            val saldo: Int? = saldoTopUp.toIntOrNull()
                            try {
                                Log.d("Saldo yang diisi", saldo.toString())
                                if (saldosementara == null)
                                    saldosementara = 0
                                val saldoBaru = saldo?.plus(saldosementara!!)
                                val stringSaldoBaru: String = saldoBaru.toString()
                                Log.d("Saldo yang baru", stringSaldoBaru)
                                val user = mutableMapOf<String, Any>()
                                user["saldo"] = stringSaldoBaru

                                mFirestore.document(userID).update(user).addOnSuccessListener {
                                    Toast.makeText(
                                        context,
                                        "Top Up Berhasil dilakukan",
                                        Toast.LENGTH_SHORT
                                    ).show()
                                    getProfile()
                                }

                            } catch (e: Exception) {
                                Toast.makeText(context, "Top Up gagal :(", Toast.LENGTH_SHORT)
                                    .show()
                            }
                        }
                }
            }
            mDialogView.btn_no_topup.setOnClickListener { view: View? ->
                mAlertDialog.dismiss()
            }
        }
    }

    fun getProfile() {
        val saldo = view?.findViewById<View>(R.id.tv_user_balance) as TextView
        val user = mAuth.currentUser
        userID = user!!.uid
        val docRef = mFirestore.document(userID)
        docRef.get()
            .addOnSuccessListener { document ->
                if (document != null) {
                    Log.d("Exist", "DocumentSnapshot data: ${document.data}")
                    saldo.text = document.getString("saldo")
                } else {
                    Log.d("No Exist", "No Document")
                }
            }
    }

    fun hitung() {
        val user = FirebaseAuth.getInstance().currentUser
        userID = user!!.uid
        val docRef = db.collection("Pengguna").document(userID)
        docRef.get()
            .addOnSuccessListener { document ->
                if (document != null) {
                    Log.d("Exist", "DocumentSnapshot data: ${document.data}")
                    val A = document.getString("yangAkanBerkurang")
                    val B = document.getString("jumlah")
                    val C = document.getString("saldo")
                    val D = document.getString("nama")
                    val E = document.getString("alamatUser")
                    val G = userID
                    val a: Int? = A?.toIntOrNull()
                    val b: Int? = B?.toIntOrNull()
                    val c: Int? = C?.toIntOrNull()
                    Log.d("Exist", "Sisa:$a")
                    Log.d("Exist", "Jumlah:$b")
                    Log.d("Exist", "Saldo:$c")

                    if(A == null){
                        Toast.makeText(context, "Silahkan isi yang diinginkan terlebih dahulu", Toast.LENGTH_LONG).show()
                    } else if(B == null){
                        Toast.makeText(context, "Silahkan isi yang diinginkan terlebih dahulu", Toast.LENGTH_LONG).show()
                    } else if(C == null){
                        Toast.makeText(context, "Silahkan isi saldo terlebih dahulu", Toast.LENGTH_LONG).show()
                    }
                    else {
                        if (a!! < b!!) {
                            val selisih = b.minus(a)
                            val biaya = selisih.times(5000).plus(6000)
                            val update = a.plus(selisih)
                            Log.d("Exist", selisih.toString())
                            Log.d("Exist", biaya.toString())
                            if (c!! > biaya) {
                                val saldoskrng = c.minus(biaya)
                                val stringsaldoskrng: String = saldoskrng.toString()
                                val selisih: String = selisih.toString()
                                val biaya: String = biaya.toString()
                                val update: String = update.toString()
                                val D: String = D.toString()
                                val E: String = E.toString()
                                val G: String = G
                                val count = 2
                                val F: String = count.toString()

                                val user = mutableMapOf<String, Any>()
                                user["saldo"] = stringsaldoskrng
                                user["yangAkanBerkurang"] = update

                                val order = mutableMapOf<String, Any>()
                                order["idUser"] = G
                                order["namaUser"] = D
                                order["jumlahYangDiperlukan"] = selisih
                                order["harga"] = biaya
                                order["alamatUser"] = E
                                order["countTimeline"] = F

                                mFirestore.document(userID).update(user).addOnSuccessListener {
                                    //                                    val id = mOrder.document().getId()
                                    mOrder.document(userID).set(order).addOnSuccessListener {
                                        Toast.makeText(
                                            context,
                                            "Pesanan ada sedang dibuat",
                                            Toast.LENGTH_LONG
                                        ).show()
                                    }
                                }
                            } else {
                                Toast.makeText(context, "Saldo anda tidak cukup", Toast.LENGTH_LONG)
                                    .show()
                            }
                        }
                    }

                } else {
                    Log.d("No Exist", "No Document")
                }

            }
    }

    companion object {
        fun getInstance(): BalanceFragment = BalanceFragment()
    }
}

package com.example.kotlindistribution.Fragment


import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.kotlindistribution.MapsActivity
import com.example.kotlindistribution.MapsTokoActivity
import com.example.kotlindistribution.Model.DeliveryDriver
import com.example.kotlindistribution.R
import com.example.kotlindistribution.adapter.DeliveryDriverAdapter
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FieldValue
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.fragment_delivery.*
import java.lang.Exception
import java.lang.reflect.Field

/**
 * A simple [Fragment] subclass.
 */
class DeliveryFragment : Fragment() {
    private var userID: String = ""

    val mAuth = FirebaseAuth.getInstance()
    val db = FirebaseFirestore.getInstance()
    val mFirestore = db.collection("Pengguna")
    val mOrder = db.collection("Orderan")
    val mTransaksi = db.collection("Transaksi")

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_delivery, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewDelivery(1)
    }

    @SuppressLint("WrongConstant")
    fun viewDelivery(size: Int) {
        val recyclerView = view?.findViewById<View>(R.id.recycler_delivery) as RecyclerView
        recyclerView.layoutManager = LinearLayoutManager(context, LinearLayout.VERTICAL, false)
        val user = FirebaseAuth.getInstance().currentUser
        userID = user!!.uid
        val docRef = mFirestore.document(userID)
        docRef.get()
            .addOnSuccessListener { document ->
                val A = document.getString("idTransaksi")
                val idTransaksi: String = A.toString()
                val docRef1 = mTransaksi.document(idTransaksi)
                docRef1.get().addOnSuccessListener { document ->
                    val driverDelivery = ArrayList<DeliveryDriver>()
                    if (document != null) {
                        val namaToko = document.getString("namaToko")
                        val alamatPemesan = document.getString("alamatPemesan")
                        val hargaPesanan = document.getString("hargaPesanan")
                        val jumlahPesanan = document.getString("jumlahPesanan")
                        if (!alamatPemesan.isNullOrEmpty()) {
                            for (i in 0 until size) {
                                val item = DeliveryDriver(
                                    "$namaToko",
                                    "$alamatPemesan",
                                    "$jumlahPesanan",
                                    "$hargaPesanan"
                                )
                                Log.d("Delivery", item.toString())
                                driverDelivery.add(item)

                                val adapter = DeliveryDriverAdapter(
                                    driverDelivery,
                                    { driverDelivery -> pilihTokoClicker(driverDelivery) },
                                    { driverDelivery -> mapItemClicked(driverDelivery) },
                                    { driverDelivery -> selesaiItemClicked(driverDelivery) })

                                recyclerView.adapter = adapter
                                recyclerView.setHasFixedSize(true)
                            }
                        } else {
                            recycler_delivery.visibility = View.INVISIBLE
                        }
                    } else {
                        Log.d("No Exist", "No Document")
                    }
                }
            }.addOnFailureListener { exception ->
                Log.d("Order", "Error getting documents: ", exception)
            }
    }

    fun pilihTokoClicker(driverDelivery: DeliveryDriver) {
        val intent = Intent(context, MapsTokoActivity::class.java)
        context?.startActivity(intent)
    }

    fun mapItemClicked(driverDelivery: DeliveryDriver) {
        val intent = Intent(context, MapsActivity::class.java)
        context?.startActivity(intent)
    }

    fun selesaiItemClicked(driverDelivery: DeliveryDriver) {
        plusSaldo()
        recycler_delivery.visibility = View.INVISIBLE
    }

    fun deleteIsi() {
        val user = FirebaseAuth.getInstance().currentUser
        userID = user!!.uid
        val docRef = mFirestore.document(userID)
        docRef.get().addOnSuccessListener {
            val updates = hashMapOf<String, Any>(
                "idTransaksi" to FieldValue.delete()
            )
            docRef.update(updates).addOnCompleteListener {
                Toast.makeText(context, "Pesanan telah selesai", Toast.LENGTH_SHORT).show()
            }
        }
    }

    fun plusSaldo() {
        val user = FirebaseAuth.getInstance().currentUser
        userID = user!!.uid
        val docRef = db.collection("Pengguna").document(userID)
        docRef.get()
            .addOnSuccessListener { document ->
                val saldoFirestore = document.getString("saldo")
                val A = document.getString("idTransaksi")
                val idTransaksi: String = A.toString()

                val docRef1 = mTransaksi.document(idTransaksi)
                docRef1.get().addOnSuccessListener { document ->
                    val idpemesan = document.getString("userIdPemesan")
                    val namadriver = document.getString("namaDriver")
                    val harga = document.getString("hargaPesanan")
                    val namatoko = document.getString("namaToko")
                    val alamatpemesan = document.getString("alamatPemesan")
                    val kapasitasB = document.getString("kapasitasBerubah")
                    val jumlahPesanan = document.getString("jumlahPesanan")
                    val saldoGaji = document.getString("hargaPesanan")

                    val kapasitas: Int? = kapasitasB?.toIntOrNull()
                    val jumlah: Int? = jumlahPesanan?.toIntOrNull()
                    var saldosementara: Int? = saldoFirestore?.toIntOrNull()
                    val saldo: Int? = saldoGaji?.toIntOrNull()
                    val idPemesan: String = idpemesan.toString()
                    val namaDriver: String = namadriver.toString()
                    val Harga: String = harga.toString()
                    val namaToko: String = namatoko.toString()
                    val alamatPemesan: String = alamatpemesan.toString()
                    Log.d("Gaji", saldo.toString())
                    try {
                        if (saldosementara == null)
                            saldosementara = 0
                        val saldoBaru = saldosementara.plus(saldo!!)
                        Log.d("Tambah", saldoBaru.toString())
                        val stringSaldoBaru: String = saldoBaru.toString()
                        Log.d("Saldo yang baru", stringSaldoBaru)
                        val kapasitasBaru = kapasitas?.plus(jumlah!!)
                        val descKapasitas = kapasitasBaru.toString()
                        Log.d("KapasitasBaru", descKapasitas)
                        val count = 5
                        val F: String = count.toString()

                        val user = mutableMapOf<String, Any>()
                        user["saldo"] = stringSaldoBaru
                        user["kapasitasBerubah"] = descKapasitas

                        val history = mutableMapOf<String, Any>()
                        history["idTransaksi"] = idTransaksi
                        history["hargaPesanan"] = Harga
                        history["namaToko"] = namaToko
                        history["alamatPemesan"] = alamatPemesan

                        val history1 = mutableMapOf<String, Any>()
                        history1["idTransaksi"] = idTransaksi
                        history1["namaDriver"] = namaDriver
                        history1["hargaPesanan"] = Harga
                        history1["namaToko"] = namaToko
                        history1["alamatPemesan"] = alamatPemesan
                        history1["countTimeLine"] = F

                        val deleteId = hashMapOf<String, Any>(
                            "idTransaksi" to FieldValue.delete()
                        )

                        mFirestore.document(userID).collection("History").document(idTransaksi)
                            .set(history).addOnSuccessListener { }

                        mFirestore.document(idPemesan).collection("History").document(idTransaksi)
                            .set(history1).addOnSuccessListener {
                                mFirestore.document(idPemesan).update(deleteId)
                            }

                        mFirestore.document(userID).update(user).addOnSuccessListener {
                            Toast.makeText(
                                context,
                                "Pendapatan bertambah",
                                Toast.LENGTH_SHORT
                            ).show()
                            mTransaksi.document(idTransaksi).delete()
                            deleteIsi()
                        }
                    } catch (e: Exception) {
                        Toast.makeText(context, "Gaji gagal masuk :(", Toast.LENGTH_SHORT).show()
                    }
                }
            }
    }
}
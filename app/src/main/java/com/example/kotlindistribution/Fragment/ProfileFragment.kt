package com.example.kotlindistribution.Fragment


import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.example.kotlindistribution.MapsActivity
import com.example.kotlindistribution.MapsUserActivity

import com.example.kotlindistribution.R
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_registrasi.view.*
import kotlinx.android.synthetic.main.fragment_profile.*
import java.lang.Exception
import android.content.Context as Context1

/**
 * A simple [Fragment] subclass.
 */
class ProfileFragment : Fragment() {
    private var userID: String = ""

    val mAuth = FirebaseAuth.getInstance()
    val db = FirebaseFirestore.getInstance()
    val mFirestore = db.collection("Pengguna")
    val mOrder = db.collection("Orderan")

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_profile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getProfile()
        hitung()

        btn_save_profile.setOnClickListener { view: View? ->
            updateProfile()
            getProfile()
        }
    }

    fun getProfile() {
        val namaText = view?.findViewById<View>(R.id.et_nama_profile) as TextView
        val emailText = view?.findViewById<View>(R.id.et_email_profile) as TextView
        val passwordText = view?.findViewById<View>(R.id.et_password_profile) as TextView
        val minimalText = view?.findViewById<View>(R.id.et_minimal_profile) as TextView
        val alamatText = view?.findViewById<View>(R.id.et_alamat_user) as TextView
        val user = FirebaseAuth.getInstance().currentUser
        userID = user!!.uid
        val docRef = db.collection("Pengguna").document(userID)
        docRef.get()
            .addOnSuccessListener { document ->
                if (document != null) {
                    Log.d("Exist", "DocumentSnapshot data: ${document.data}")
                    namaText.text = document.getString("nama")
                    emailText.text = document.getString("email")
                    passwordText.text = document.getString("password")
                    minimalText.text = document.getString("jumlah")
                    alamatText.text = document.getString("alamatUser")
                } else {
                    Log.d("No Exist", "No Document")
                }
            }
    }

    fun updateProfile(){
        val namaText = view?.findViewById<View>(R.id.et_nama_profile) as EditText
        val emailText = view?.findViewById<View>(R.id.et_email_profile) as EditText
        val passwordText = view?.findViewById<View>(R.id.et_password_profile) as EditText
        val jumlahPermintaan = view?.findViewById<View>(R.id.et_minimal_profile) as EditText
        val alamatText = view?.findViewById<View>(R.id.et_alamat_user) as EditText

        val nama = namaText.text.toString().trim()
        val email = emailText.text.toString().trim()
        val password = passwordText.text.toString().trim()
        val minimal = jumlahPermintaan.text.toString().trim()
        val alamat = alamatText.text.toString().trim()
        val user = FirebaseAuth.getInstance().currentUser

        if(nama.isEmpty()){
            Toast.makeText(context,"Tolong isi Nama anda", Toast.LENGTH_SHORT).show()
        } else if(email.isEmpty()){
            Toast.makeText(context,"Tolong isi Email anda", Toast.LENGTH_SHORT).show()
        } else if(password.isEmpty()){
            Toast.makeText(context,"Tolong isi Password", Toast.LENGTH_SHORT).show()
        } else if(minimal.isEmpty()){
            Toast.makeText(context,"Tolong isi Jumlah yang diinginkan", Toast.LENGTH_SHORT).show()
        } else if(alamat.isEmpty()){
            Toast.makeText(context,"Tolong isi Alamat anda", Toast.LENGTH_SHORT).show()
        }
        else{
            user?.updateEmail(email)
                ?.addOnCompleteListener{ task ->
                    if (task.isSuccessful) {
                        Log.d("Profile", "User email address updated.")
                    }
                }
            user?.updatePassword(password)
                ?.addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        Log.d("Profile", "User password updated.")
                    }
                }

            userID = user!!.uid
            try{
                Log.d("jumlah yang diinginkan", minimal)
                val user = mutableMapOf<String, Any>()
                user["nama"] = nama
                user["email"] = email
                user["password"] = password
                user["jumlah"] = minimal
                user["yangAkanBerkurang"] = minimal
                user["alamatUser"] = alamat

                mFirestore.document(userID).update(user).addOnSuccessListener{
                    Toast.makeText(context, "Sukses", Toast.LENGTH_SHORT).show()
                }

            } catch (e: Exception){
                Toast.makeText(context, "Gagal", Toast.LENGTH_SHORT).show()
            }
        }
    }

    fun hitung(){
        val user = FirebaseAuth.getInstance().currentUser
        userID = user!!.uid
        val docRef = db.collection("Pengguna").document(userID)
        docRef.get()
            .addOnSuccessListener { document ->
                if (document != null) {
                    Log.d("Exist", "DocumentSnapshot data: ${document.data}")
                    val A = document.getString("yangAkanBerkurang")
                    val B = document.getString("jumlah")
                    val C = document.getString("saldo")
                    val D = document.getString("nama")
                    val E = document.getString("alamatUser")
                    val G = userID
                    val a: Int? = A?.toIntOrNull()
                    val b: Int? = B?.toIntOrNull()
                    val c: Int? = C?.toIntOrNull()
                    Log.d("Exist", "Sisa:$a")
                    Log.d("Exist", "Jumlah:$b")
                    Log.d("Exist", "Saldo:$c")

                    if(A == null){
                        Toast.makeText(context, "Silahkan isi yang diinginkan terlebih dahulu", Toast.LENGTH_LONG).show()
                    } else if(B == null){
                        Toast.makeText(context, "Silahkan isi yang diinginkan terlebih dahulu", Toast.LENGTH_LONG).show()
                    } else if(C == null){
                        Toast.makeText(context, "Silahkan isi saldo terlebih dahulu", Toast.LENGTH_LONG).show()
                    }
                    else {
                        if(a!! < b!! ){
                            val selisih = b.minus(a)
                            val biaya = selisih.times(5000).plus(6000)
                            val update = a.plus(selisih)
                            Log.d("Exist", selisih.toString())
                            Log.d("Exist", biaya.toString())
                            if(c!! > biaya){
                                val saldoskrng = c.minus(biaya)
                                val stringsaldoskrng:String = saldoskrng.toString()
                                val selisih:String = selisih.toString()
                                val biaya:String = biaya.toString()
                                val update:String = update.toString()
                                val D:String = D.toString()
                                val E:String = E.toString()
                                val G:String = G
                                val count = 2
                                val F: String = count.toString()

                                val user = mutableMapOf<String, Any>()
                                user["saldo"] = stringsaldoskrng
                                user["yangAkanBerkurang"] = update

                                val order = mutableMapOf<String, Any>()
                                order["idUser"] = G
                                order["namaUser"] = D
                                order["jumlahYangDiperlukan"] = selisih
                                order["harga"] = biaya
                                order["alamatUser"] = E
                                order["countTimeline"] = F

                                mFirestore.document(userID).update(user).addOnSuccessListener {
                                    //                                    val id = mOrder.document().getId()
                                    mOrder.document(userID).set(order).addOnSuccessListener {
                                        Toast.makeText(
                                            context,
                                            "Pesanan ada sedang dibuat",
                                            Toast.LENGTH_LONG
                                        ).show()
                                    }
                                }
                            }
                            else {
                                Toast.makeText(context, "Saldo anda tidak cukup", Toast.LENGTH_LONG).show()
                            }
                        }
                    }

                } else {
                    Log.d("No Exist", "No Document")
                }

            }
    }

    companion object {
        fun getInstance(): ProfileFragment = ProfileFragment()
    }
}


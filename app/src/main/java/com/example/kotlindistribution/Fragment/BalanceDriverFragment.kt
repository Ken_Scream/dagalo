package com.example.kotlindistribution.Fragment


import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast

import com.example.kotlindistribution.R
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import java.lang.Exception

/**
 * A simple [Fragment] subclass.
 */
class BalanceDriverFragment : Fragment() {
    private var userID: String = ""

    val mAuth = FirebaseAuth.getInstance()
    val db = FirebaseFirestore.getInstance()
    val mFirestore = db.collection("Pengguna")
    val mOrder = db.collection("Orderan")

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_balance_driver, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getSaldo()
//        plusSaldo()
    }

    fun plusSaldo() {
        val user = FirebaseAuth.getInstance().currentUser
        userID = user!!.uid
        val docRef = db.collection("Pengguna").document(userID)
        docRef.get()
            .addOnSuccessListener { document ->
                val saldoFirestore = document.getString("saldo")
                var saldosementara: Int? = saldoFirestore?.toIntOrNull()
                val saldoGaji = document.getString("hargaPesanan")
                val saldo: Int? = saldoGaji?.toIntOrNull()
                try {
                    if (saldosementara == null)
                        saldosementara = 0
                    val saldoBaru = saldosementara.plus(saldo!!)
                    val stringSaldoBaru: String = saldoBaru.toString()
                    Log.d("Saldo yang baru", stringSaldoBaru)
                    val user = mutableMapOf<String, Any>()
                    user["saldo"] = stringSaldoBaru

                    mFirestore.document(userID).update(user).addOnSuccessListener {
                        Toast.makeText(
                            context,
                            "Pendapatan bertambah",
                            Toast.LENGTH_SHORT
                        ).show()
                        getSaldo()
                    }
                } catch (e: Exception) {
                    Toast.makeText(context, "Gaji gagal masuk :(", Toast.LENGTH_SHORT).show()
                }
            }
    }

    fun getSaldo() {
        val saldo = view?.findViewById<View>(R.id.tv_driver_nominal) as TextView
        val user = mAuth.currentUser
        userID = user!!.uid
        val docRef = mFirestore.document(userID)
        docRef.get()
            .addOnSuccessListener { document ->
                if (document != null) {
                    Log.d("Exist", "DocumentSnapshot data: ${document.data}")
                    saldo.text = document.getString("saldo")
                } else {
                    Log.d("No Exist", "No Document")
                }
            }
    }
}
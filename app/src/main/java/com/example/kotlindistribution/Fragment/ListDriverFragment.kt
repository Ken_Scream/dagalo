package com.example.kotlindistribution.Fragment

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.Switch
import android.widget.TextView
import android.widget.Toast
import com.example.kotlindistribution.R
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.fragment_list_driver.*
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.kotlindistribution.HomePageDriverActivity
import com.example.kotlindistribution.LoginDriverActivity
import com.example.kotlindistribution.Model.ListDriver
import com.example.kotlindistribution.adapter.ListDriverAdapter
import kotlinx.android.synthetic.main.dialog_aktivasi.view.*
import kotlinx.android.synthetic.main.dialog_takeorder.*
import kotlinx.android.synthetic.main.dialog_takeorder.view.*
import kotlinx.android.synthetic.main.item_list.*
import java.lang.Exception
import kotlin.collections.ArrayList

/**
 * A simple [Fragment] subclass.
 */
class ListDriverFragment : Fragment() {
    private var userID: String = ""

    val mAuth = FirebaseAuth.getInstance()
    val db = FirebaseFirestore.getInstance()
    val mFirestore = db.collection("Pengguna")
    val mOrder = db.collection("Orderan")
    val mTransaksi = db.collection("Transaksi")

    private lateinit var textViewData: TextView
    private lateinit var usernameTextDriver: TextView
    private lateinit var swicthOnOff: Switch

    @SuppressLint("WrongConstant")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_list_driver, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        switch()
    }

    @SuppressLint("WrongConstant")
    fun viewData(size: Int) {
//        textViewData = view?.findViewById<View>(R.id.tv_view_data) as TextView
        val recyclerView = view?.findViewById<View>(R.id.recycler_list) as RecyclerView
        recyclerView.layoutManager = LinearLayoutManager(context, LinearLayout.VERTICAL, false)
        mOrder.get().addOnSuccessListener { result ->
            val listDriver = ArrayList<ListDriver>()
            for (document in result) {
                val id = document.getString("idUser")
                val nama = document.getString("namaUser")
                val jumlah = document.getString("jumlahYangDiperlukan")
                val harga = document.getString("harga")
                val alamat = document.getString("alamatUser")
                val namaToko = document.getString("namaToko")
                val countTimeline = document.getString ("countTimeLine")
                Log.d("Order", "${document.id} => ${document.data}")
                for (i in 0 until size) {
                    val item = ListDriver(
                        "$id",
                        "$nama",
                        "$jumlah",
                        "$harga",
                        "$alamat",
                        "$namaToko",
                        "$countTimeline"
                    )
                    Log.d("Order1", item.toString())
                    listDriver.add(item)

                    val adapter = ListDriverAdapter(
                        listDriver,
                        { listDriver -> listItemClicked(listDriver) },
                        { listDriver -> listDeleteClicked(listDriver) })

                    recyclerView.adapter = adapter
                    recyclerView.setHasFixedSize(true)
                }
            }
        }.addOnFailureListener { exception ->
            Log.d("Order", "Error getting documents: ", exception)
        }
    }

    private fun listDeleteClicked(listDriver: ListDriver) {
        btn_take.setOnClickListener { view: View? ->
            val mDialogView = LayoutInflater.from(context).inflate(R.layout.dialog_takeorder, null)
            val mBuilder = AlertDialog.Builder(context)
                .setView(mDialogView)
                .setTitle("Apakah anda sudah yakin?")
            val mAlertDialog = mBuilder.show()

            mDialogView.btn_yes_ambil.setOnClickListener { view: View? ->
                mAlertDialog.dismiss()
                val user = FirebaseAuth.getInstance().currentUser
                userID = user!!.uid
                val docRef = mFirestore.document(userID)
                docRef.get().addOnSuccessListener { document ->
                    if (document != null) {
                        val A = document.getString("kapasitasBerubah")
                        val B = document.getString("nama")
                        val C = userID
                        val a: Int? = A?.toIntOrNull()
                        try {
                            if (A == null)
                                Toast.makeText(
                                    context, "Silahkan Isi Kapasitas Kendaraan Anda",
                                    Toast.LENGTH_SHORT
                                ).show()
                            else {
                                //dataUser
                                val descId = listDriver.id
                                val descNama = listDriver.nama
                                val descJumlah = listDriver.jumlah
                                val descHarga = listDriver.harga
                                val descAlamat = listDriver.alamat
                                Log.d(
                                    "deskripsi",
                                    descNama + descJumlah + descHarga + descAlamat
                                )
                                val jumlahPesanan: Int? = descJumlah.toIntOrNull()
                                if (jumlahPesanan!! <= a!!) {
                                    val jumlahBaru = a.minus(jumlahPesanan)
                                    val kapasitasBaru: String = jumlahBaru.toString()
                                    val namaDriver: String = B.toString()
                                    val idTransaksi = mTransaksi.document().id
                                    val count = 3
                                    val F: String = count.toString()

                                    val desc = mutableMapOf<String, Any>()
                                    desc["userIdPemesan"] = descId
                                    desc["namaPemesan"] = descNama
                                    desc["jumlahPesanan"] = descJumlah
                                    desc["hargaPesanan"] = descHarga
                                    desc["alamatPemesan"] = descAlamat
                                    desc["kapasitasBerubah"] = kapasitasBaru
                                    desc["namaDriver"] = namaDriver
                                    desc["idDriver"] = C
                                    desc["idTransaksi"] = idTransaksi
                                    desc["countTimeLine"] = F


                                    mTransaksi.document(idTransaksi).set(desc).addOnSuccessListener {
//                                        Log.d("Transaksi", idTransaksi)
                                        val descU = mutableMapOf<String, Any>()
                                        descU["idTransaksi"] = idTransaksi

                                        mFirestore.document(userID).update(descU).addOnSuccessListener {
                                            Toast.makeText(context, "Sukses", Toast.LENGTH_SHORT)
                                                .show()
                                        }
                                        mFirestore.document(descId).update(descU)
                                    }
//                                    mFirestore.document(userID).update(desc).addOnSuccessListener {
//                                    }
                                    mOrder.document(descId).delete()
                                    viewData(1)
                                } else
                                    Toast.makeText(
                                        context,
                                        "Pesanan tidak sesuai kapasitas",
                                        Toast.LENGTH_SHORT
                                    ).show()
                            }
                        } catch (e: Exception) {
                            Toast.makeText(context, "Pesanan gagal diambil", Toast.LENGTH_SHORT)
                                .show()
                        }
                    }
                }
            }
            mDialogView.btn_no_ambil.setOnClickListener { view: View? ->
                mAlertDialog.dismiss()
            }
        }
    }

    fun listItemClicked(listDriver: ListDriver) {
        Toast.makeText(context, "Clicked: ${listDriver.nama}", Toast.LENGTH_LONG).show()
    }

    //PUNYA DRIVER
    fun switch() {
        swicthOnOff = view?.findViewById(R.id.sw_onOff) as Switch
        sw_onOff.setOnCheckedChangeListener { compoundButton, onSwitch ->
            if (onSwitch) {
                viewData(1)
                recycler_list.visibility = View.VISIBLE
            } else {
                recycler_list.visibility = View.INVISIBLE
            }
        }
    }
    //SAMPAI SINI
}